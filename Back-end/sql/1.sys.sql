--DROP TABLESPACE ISS INCLUDING CONTENTS AND DATAFILES;
--DROP ROLE ISS_DBA;

CREATE TABLESPACE iss DATAFILE 'iss.dbf' SIZE 20m AUTOEXTEND ON;
-- Tạo User chứa dữ liệu
CREATE USER hr IDENTIFIED BY 123456 DEFAULT TABLESPACE iss QUOTA UNLIMITED ON iss;
GRANT connect, resource TO hr;
-- Tạo user chịu trách nhiệm quản lý User Label
CREATE USER hr_sec IDENTIFIED BY 123456 DEFAULT TABLESPACE iss QUOTA UNLIMITED ON iss;
GRANT connect, create user, drop user, create role, drop any role, create procedure TO hr_sec WITH ADMIN OPTION;
GRANT select ON dba_users TO hr_sec;
-- Tạo user chịu trách nhiệm quản lý Data Label
CREATE USER sec_admin IDENTIFIED BY 123456 DEFAULT TABLESPACE iss QUOTA UNLIMITED ON iss;
GRANT connect, create procedure TO sec_admin;
GRANT execute ON dbms_rls TO sec_admin;
GRANT select ON dba_role_privs TO sec_admin;
--select * from dba_recyclebin;
--purge dba_recyclebin;

COMMIT;
