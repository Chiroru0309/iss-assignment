--CONN lbacsys/lbacsys;
EXEC SA_SYSDBA.CREATE_POLICY(policy_name => 'ACCESS_EMP',column_name => 'ols_emp');
--EXEC SA_SYSDBA.DROP_POLICY('ACCESS_EMP', TRUE);
--EXEC SA_SYSDBA.ENABLE_POLICY('ACCESS_EMP');
--EXEC SA_SYSDBA.DISABLE_POLICY('ACCESS_EMP');
GRANT access_emp_dba TO sec_admin;
-- Package dùng để tạo ra label, compartment
GRANT execute ON sa_components TO sec_admin;
-- Package dùng để tạo các label
GRANT execute ON sa_label_admin TO sec_admin;
-- Package dùng để gán chính sách cho các table/schema (Data Label)
GRANT execute ON sa_policy_admin TO sec_admin;
-- Package chuyển string thành label
GRANT execute ON to_lbac_data_label TO sec_admin WITH GRANT OPTION;

GRANT access_emp_dba TO hr_sec;
-- Package dùng để gán các label cho user (User Label)
GRANT execute ON sa_user_admin TO hr_sec;

COMMIT;
--
--SELECT * FROM DBA_SA_USERS;
--SELECT * FROM DBA_SA_USER_LEVELS;
--SELECT * FROM DBA_SA_USER_COMPARTMENTS;
--SELECT * FROM DBA_SA_USER_GROUPS;
