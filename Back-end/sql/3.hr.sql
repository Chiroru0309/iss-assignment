--SELECT * FROM hr.employee;
--SELECT * FROM hr.department;
--DROP TABLE hr.employee;
--DROP TABLE hr.department;
--TRUNCATE TABLE hr.employee;
--DROP SEQUENCE dept_seq;
--DROP SEQUENCE emp_seq;
CREATE SEQUENCE dept_seq START WITH 1;
CREATE TABLE department (
  id        NUMBER(10)      DEFAULT dept_seq.nextval NOT NULL,
  name      VARCHAR2(255)   NOT NULL,
  CONSTRAINT dept_pk PRIMARY KEY (id)
);

CREATE SEQUENCE emp_seq START WITH 1;
CREATE TABLE employee (
  id            NUMBER(15)      DEFAULT emp_seq.nextval NOT NULL,
  username      VARCHAR2(50)    NOT NULL,
  name          VARCHAR2(50)    NOT NULL,
  birth         DATE            NOT NULL,
  email         VARCHAR2(255)   NOT NULL,
  department_id NUMBER(10)      NOT NULL,
  position      VARCHAR2(255)   NOT NULL,
  is_manager    NUMBER(1,0)     DEFAULT 0 NOT NULL,
  taxcode       VARCHAR2(255)   NOT NULL,
  salary        NUMBER(15)      NOT NULL,
  CONSTRAINT pk PRIMARY KEY (id),
  CONSTRAINT fk_department FOREIGN KEY (department_id) REFERENCES department(id)
);

INSERT INTO department(name) VALUES ('FINANCE');
INSERT INTO department(name) VALUES ('HUMAN RESOURCE');
INSERT INTO department(name) VALUES ('SALES AND MARKETING');
INSERT INTO department(name) VALUES ('TECHNICAL');
--INSERT INTO employee(username,name,birth,email,department_id,position,is_manager,taxcode,salary) VALUES ('user01','user01', DATE '1996-11-14','191919@hcmut.edu.vn',1,'Manager',1,'FX191919',15000);
--INSERT INTO employee(username,name,birth,email,department_id,position,is_manager,taxcode,salary) VALUES ('user02','user02', DATE '1997-11-14','191918@hcmut.edu.vn',2,'Manager',1,'FX191918',14000);
--INSERT INTO employee(username,name,birth,email,department_id,position,is_manager,taxcode,salary) VALUES ('user03','user03', DATE '1998-11-14','191917@hcmut.edu.vn',3,'Manager',1,'FX191917',13000);
--INSERT INTO employee(username,name,birth,email,department_id,position,is_manager,taxcode,salary) VALUES ('user04','user04', DATE '1999-11-14','191916@hcmut.edu.vn',4,'Manager',1,'FX191916',12000);
--INSERT INTO employee(username,name,birth,email,department_id,position,is_manager,taxcode,salary) VALUES ('user05','user05', DATE '2000-11-14','191915@hcmut.edu.vn',1,'Manager',0,'FX191915',11000);
--INSERT INTO employee(username,name,birth,email,department_id,position,is_manager,taxcode,salary) VALUES ('user06','user06', DATE '1995-11-14','191914@hcmut.edu.vn',2,'Manager',0,'FX191914',16000);
--INSERT INTO employee(username,name,birth,email,department_id,position,is_manager,taxcode,salary) VALUES ('user07','user07', DATE '1996-12-14','191913@hcmut.edu.vn',3,'Normal',0,'FX191913',9000);
--INSERT INTO employee(username,name,birth,email,department_id,position,is_manager,taxcode,salary) VALUES ('user08','user08', DATE '1996-11-11','191929@hcmut.edu.vn',4,'Normal',0,'FX191912',8000);
--INSERT INTO employee(username,name,birth,email,department_id,position,is_manager,taxcode,salary) VALUES ('user09','user09', DATE '1996-10-14','191939@hcmut.edu.vn',1,'Normal',0,'FX191911',7000);
--INSERT INTO employee(username,name,birth,email,department_id,position,is_manager,taxcode,salary) VALUES ('user10','user10', DATE '1996-11-20','191949@hcmut.edu.vn',2,'Normal',0,'FX191910',6000);
--INSERT INTO employee(username,name,birth,email,department_id,position,is_manager,taxcode,salary) VALUES ('user11','user11', DATE '1996-10-21','191959@hcmut.edu.vn',3,'Normal',0,'FX191909',5000);
--INSERT INTO employee(username,name,birth,email,department_id,position,is_manager,taxcode,salary) VALUES ('user12','user12', DATE '1996-10-21','191959@hcmut.edu.vn',4,'Normal',0,'FX191019',10000);

GRANT select, insert, update ON hr.employee TO sec_admin;
GRANT select, insert, update, delete ON hr.employee TO hr_sec WITH GRANT OPTION;
GRANT select, alter ON hr.emp_seq TO hr_sec WITH GRANT OPTION;
GRANT select ON hr.department TO hr_sec WITH GRANT OPTION;
COMMIT;
