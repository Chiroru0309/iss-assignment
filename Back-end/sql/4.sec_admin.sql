--CONN sec_admin/123456;
EXEC SA_COMPONENTS.CREATE_LEVEL('ACCESS_EMP',1000,'PUB','PUBLIC');
EXEC SA_COMPONENTS.CREATE_LEVEL('ACCESS_EMP',2000,'CONF','CONFIDENTIAL');
EXEC SA_COMPONENTS.CREATE_LEVEL('ACCESS_EMP',3000,'SENS','SENSITIVE');
EXEC SA_COMPONENTS.CREATE_LEVEL('ACCESS_EMP',4000,'HS','HIGHLY SECRET');
EXEC SA_COMPONENTS.CREATE_COMPARTMENT('ACCESS_EMP',1000,'FIN','FINACE');
EXEC SA_COMPONENTS.CREATE_COMPARTMENT('ACCESS_EMP',2000,'HR','HUMAN RESOURCE');
EXEC SA_COMPONENTS.CREATE_COMPARTMENT('ACCESS_EMP',3000,'SM','SALES AND MARKETING');
EXEC SA_COMPONENTS.CREATE_COMPARTMENT('ACCESS_EMP',4000,'TECH','TECHNICAL');
EXEC SA_LABEL_ADMIN.CREATE_LABEL('ACCESS_EMP',10000,'PUB');
EXEC SA_LABEL_ADMIN.CREATE_LABEL('ACCESS_EMP',20000,'CONF');
EXEC SA_LABEL_ADMIN.CREATE_LABEL('ACCESS_EMP',20100,'CONF:FIN');
EXEC SA_LABEL_ADMIN.CREATE_LABEL('ACCESS_EMP',20200,'CONF:HR');
EXEC SA_LABEL_ADMIN.CREATE_LABEL('ACCESS_EMP',20300,'CONF:SM');
EXEC SA_LABEL_ADMIN.CREATE_LABEL('ACCESS_EMP',20400,'CONF:TECH');
EXEC SA_LABEL_ADMIN.CREATE_LABEL('ACCESS_EMP',21100,'CONF:FIN,SM,TECH');
EXEC SA_LABEL_ADMIN.CREATE_LABEL('ACCESS_EMP',21500,'CONF:FIN,HR,SM,TECH');
EXEC SA_LABEL_ADMIN.CREATE_LABEL('ACCESS_EMP',30000,'SENS');
EXEC SA_LABEL_ADMIN.CREATE_LABEL('ACCESS_EMP',40000,'HS');

-- Áp dụng chính sách OLS
-- BEGIN
--     SA_POLICY_ADMIN.APPLY_TABLE_POLICY(
--         policy_name => 'ACCESS_EMP',
--         schema_name => 'HR',
--         table_name => 'EMPLOYEE',
--         table_options => 'NO_CONTROL'
--     );
-- END;
-- /

-- BEGIN
--     SA_POLICY_ADMIN.REMOVE_TABLE_POLICY(
--         policy_name => 'ACCESS_EMP',
--         schema_name => 'HR',
--         table_name => 'EMPLOYEE',
--         drop_column => TRUE
--     );
-- END;
-- /

CREATE OR REPLACE FUNCTION sec_admin.gen_emp_label(p_dept_id NUMBER)
RETURN LBACSYS.LBAC_LABEL
AS
    i_label varchar2(80);
BEGIN
    IF p_dept_id = 1 THEN
        i_label := 'CONF:FIN';
    ELSIF p_dept_id = 2 THEN
        i_label := 'CONF:HR';
    ELSIF p_dept_id = 3 THEN
        i_label := 'CONF:SM';
    ELSIF p_dept_id = 4 THEN
        i_label := 'CONF:TECH';
    ELSE
        i_label := 'CONF';
    END IF;
    RETURN TO_LBAC_DATA_LABEL('ACCESS_EMP', i_label);
END;
/
GRANT execute ON sec_admin.gen_emp_label TO lbacsys;

BEGIN
    SA_POLICY_ADMIN.APPLY_TABLE_POLICY(
        policy_name => 'ACCESS_EMP',
        schema_name => 'HR',
        table_name => 'EMPLOYEE',
        table_options => 'HIDE,READ_CONTROL,WRITE_CONTROL',
        label_function => 'sec_admin.gen_emp_label(:new.department_id)',
        predicate => NULL
    );
END;
/

-- Áp dụng Row-Level Security
CREATE OR REPLACE FUNCTION user_only(
    p_schema IN VARCHAR2 DEFAULT NULL
    ,p_object IN VARCHAR2 DEFAULT NULL
) RETURN VARCHAR2
AS
    is_access_salary NUMBER;
BEGIN
    SELECT COUNT(*) INTO is_access_salary FROM DBA_ROLE_PRIVS WHERE grantee=USER AND granted_role='ACCESS_SALARY';
    IF is_access_salary > 0 THEN
        RETURN '1=1';
    END IF;
    RETURN 'UPPER(username)=user';
END;
/

-- BEGIN
--     DBMS_RLS.drop_policy(
--         object_schema => 'HR',
--         object_name => 'EMPLOYEE',
--         policy_name => 'hide_salary'
--     );
-- END;
-- /

BEGIN
    DBMS_RLS.add_policy(
        object_schema => 'HR',
        object_name => 'EMPLOYEE',
        policy_name => 'hide_salary',
        function_schema => 'sec_admin',
        policy_function => 'user_only',
        statement_types => 'SELECT',
        sec_relevant_cols => 'SALARY',
        sec_relevant_cols_opt => DBMS_RLS.all_rows
    );
END;
/

COMMIT;
