CREATE ROLE emp_role;
GRANT connect TO emp_role;
GRANT select ON hr.employee TO emp_role;
GRANT select ON hr.department TO emp_role;
--DROP ROLE emp_role;
CREATE ROLE hr_role;
GRANT connect TO hr_role;
GRANT select, insert, update, delete ON hr.employee TO hr_role;
GRANT select, alter ON hr.emp_seq TO hr_role;
--DROP ROLE hr_role;

CREATE ROLE access_salary;
GRANT access_salary TO hr;
GRANT access_salary TO sec_admin;

BEGIN
    sa_user_admin.set_user_privs(
        policy_name => 'ACCESS_EMP',
        user_name => 'HR',
        PRIVILEGES => 'FULL'
    );
END;
/

BEGIN
    sa_user_admin.set_user_privs(
        policy_name => 'ACCESS_EMP',
        user_name => 'HR_SEC',
        PRIVILEGES => 'FULL'
    );
END;
/

BEGIN
    sa_user_admin.set_user_privs(
        policy_name => 'ACCESS_EMP',
        user_name => 'SEC_ADMIN',
        PRIVILEGES => 'FULL'
    );
END;
/

CREATE OR REPLACE PROCEDURE revoke_all_role(
    p_username VARCHAR2
)
AS
    v_is_manager NUMBER;
    v_dept_id NUMBER;
BEGIN
    SELECT department_id, is_manager INTO v_dept_id, v_is_manager FROM hr.employee WHERE UPPER(username)=UPPER(p_username);
    EXECUTE IMMEDIATE 'REVOKE emp_role FROM '||p_username;
    IF v_dept_id=2 THEN
        EXECUTE IMMEDIATE 'REVOKE hr_role FROM '||p_username;
    END IF;
    IF v_is_manager=1 THEN
        EXECUTE IMMEDIATE 'REVOKE access_salary FROM '||p_username;
    END IF;
END;
/

CREATE OR REPLACE PROCEDURE grant_role(
    p_username VARCHAR2
    ,p_dept_id NUMBER
    ,p_is_manager NUMBER
    ,p_is_revoke NUMBER DEFAULT NULL
)
AS
    user_max_read_label VARCHAR2(20);
    user_max_write_label VARCHAR2(20);
    user_min_write_label VARCHAR2(20);
    user_def_label VARCHAR2(20);
    user_row_label VARCHAR2(20);
    user_exist INTEGER;
BEGIN
    SELECT COUNT(*) INTO user_exist FROM dba_users WHERE UPPER(username)=UPPER(p_username);
    IF user_exist=0 THEN
        RAISE_APPLICATION_ERROR(-20001,'User does not exists.');
    END IF;
    IF p_is_revoke=1 THEN
        hr_sec.revoke_all_role(p_username);
    END IF;
    EXECUTE IMMEDIATE 'GRANT emp_role TO '||p_username;
    user_max_read_label := 'CONF';
    user_max_write_label := 'CONF';
    user_min_write_label := 'PUB';
    user_def_label := 'CONF';
    user_row_label := 'CONF';
    IF p_dept_id=1 THEN
        user_max_read_label := 'CONF:FIN,HR,SM,TECH';
        user_def_label := user_max_read_label;
        EXECUTE IMMEDIATE 'GRANT access_salary TO '||p_username;
    ELSIF p_dept_id=2 THEN
    BEGIN
        EXECUTE IMMEDIATE 'GRANT hr_role TO '||p_username;
        EXECUTE IMMEDIATE 'GRANT access_salary TO '||p_username;
        user_max_read_label := 'CONF:FIN,HR,SM,TECH';
        user_def_label := user_max_read_label;
        IF p_is_manager = 1 THEN
            user_max_write_label := 'CONF:FIN,HR,SM,TECH';
        ELSE
            user_max_write_label := 'CONF:FIN,SM,TECH';
        END IF;
    END;
    ELSIF p_dept_id=3 THEN
        user_max_read_label := 'CONF:SM';
        user_def_label := user_max_read_label;
    ELSIF p_dept_id=4 THEN
        user_max_read_label := 'CONF:TECH';
        user_def_label := user_max_read_label;
    END IF;
    IF p_is_manager=1 THEN
        EXECUTE IMMEDIATE 'GRANT access_salary TO '||p_username;
    END IF;
    BEGIN
        sa_user_admin.set_user_labels(
            policy_name => 'ACCESS_EMP',
            user_name => p_username,
            max_read_label => user_max_read_label,
            max_write_label => user_max_write_label,
            min_write_label => user_min_write_label,
            def_label => user_def_label,
            row_label => user_row_label
        );
    END;
END;
/

CREATE OR REPLACE PROCEDURE add_user(
    p_username VARCHAR2
    ,p_password VARCHAR2
    ,p_dept_id NUMBER
    ,p_is_manager NUMBER
)
AS
    user_exist NUMBER;
BEGIN
    SELECT COUNT(*) INTO user_exist FROM dba_users WHERE username=UPPER(p_username);
    IF user_exist > 0 THEN
        RAISE_APPLICATION_ERROR(-20001,'User already exist');
    END IF;

    EXECUTE IMMEDIATE 'CREATE USER '||p_username||' IDENTIFIED BY '||p_password;
    hr_sec.grant_role(p_username,p_dept_id,p_is_manager);
END;
/

CREATE OR REPLACE PROCEDURE remove_employee(
    p_username VARCHAR2
)
AS
BEGIN
    DELETE FROM hr.employee WHERE username = p_username;
    -- IF SQL%rowcount > 0 THEN
        EXECUTE IMMEDIATE 'DROP USER '||p_username;
    -- END IF;
END;
/

--EXEC hr_sec.add_user('user01','user01',1,1);
--EXEC hr_sec.add_user('user02','user02',2,1);
--EXEC hr_sec.add_user('user03','user03',3,1);
--EXEC hr_sec.add_user('user04','user04',4,1);
--EXEC hr_sec.add_user('user05','user05',1,0);
--EXEC hr_sec.add_user('user06','user06',2,0);
--EXEC hr_sec.add_user('user07','user07',3,0);
--EXEC hr_sec.add_user('user08','user08',4,0);
--EXEC hr_sec.add_user('user09','user09',1,0);
--EXEC hr_sec.add_user('user10','user10',2,0);
--EXEC hr_sec.add_user('user11','user11',3,0);
--EXEC hr_sec.add_user('user12','user12',4,0);

COMMIT;
