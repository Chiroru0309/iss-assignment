module.exports = {
  connectString: process.env.connectString,
  hr_opts: {
    user: process.env.hr_user,
    password: process.env.hr_password,
    connectString: process.env.connectString
  },
  loginMaxAge: 24 * 60 * 60 * 1000,
  serviceUrl: process.env.serviceUrl,
  jwtSecret: process.env.jwtSecret,
  cookieSecret: process.env.cookieSecret
}
