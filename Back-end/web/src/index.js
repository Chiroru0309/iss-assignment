const config = require('./config')
const qs = require('qs')
const path = require('path')
const urljoin = require('url-join')
const cookieSession = require('cookie-session')
const bodyParser = require('body-parser')
const express = require('express')
const { verifyUserLogin, redirectError, redirectSuccess, verifySession, logout } = require('./login')
const dashboard_router = require('./router/dashboard_router.js')
const api_router = require('./router/api_router.js')

const app = express()

app.set('view engine', 'ejs')
app.set('views', path.join(__dirname, 'views'))
app.use(express.static(path.join(__dirname, 'static')))
app.use(
  cookieSession({
    name: 'qlnv',
    secret: config.cookieSecret,
    signed: true,
    maxAge: 24 * 60 * 60 * 1000
  })
)

app.get('/', verifySession, (req, res) => redirectSuccess(res, req.query))

app.get('/login', (req, res) => {
  const error = req.query.error
  if (req.session) {
    const sessionExired = req.session.expired || 0
    const now = new Date().getTime()
    if (now < sessionExired) {
      return redirectSuccess(res)
    }
    req.session = null
  }
  res.render('login', { error })
})

app.post('/login', bodyParser.urlencoded({ extended: false }), verifyUserLogin, (req, res) => redirectSuccess(res))

app.get('/logout', (req, res) => {
  delete req.query.error
  return logout(req, res)
})

app.use('/dashboard', verifySession, dashboard_router)
app.use('/api', bodyParser.urlencoded({ extended: false }), api_router)

const listener = app.listen(3000, () => {
  console.log('Listening on port ' + listener.address().port)
})
