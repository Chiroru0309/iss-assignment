const config = require('./config')
const oracleClient = require('./oracleClient')
const qs = require('qs')
const urljoin = require('url-join')
const asyncMiddleware = require('./middleware/asyncMiddleware')
const logger = require('./utils/createLogger')('login')

const loginError = {
  INVALID_REQUEST: 1,
  AUTH_FAIL: 2,
  INVALID_TICKET: 3,
  SESSION_EXPIRED: 4,
  INTERNAL_ERROR: 5
}

const redirectError = function(res, query = {}, error = 0) {
  if (error > 0) {
    query.error = error
  }
  res.redirect(urljoin(config.serviceUrl, 'login', qs.stringify(query, { addQueryPrefix: true })))
}

const redirectSuccess = function(res, query = {}) {
  res.redirect(urljoin(config.serviceUrl, 'dashboard', qs.stringify(query, { addQueryPrefix: true })))
}

const verifySession = function(req, res, next) {
  if (req.session.expired) {
    const now = new Date().getTime()
    if (now < req.session.expired) {
      return next()
    }
    req.session = null
    return redirectError(res, req.query, loginError.SESSION_EXPIRED)
  }
  return redirectError(res, req.query)
}

const verifyUserLogin = asyncMiddleware(async (req, res, next) => {
  const opts = {
    user: req.body.username,
    password: req.body.password,
    connectString: config.connectString
  }
  try {
    const result = await oracleClient(opts).getEmployee(req.body.username)
    if (result.rows.length > 0) {
      req.session.username = req.body.username
      req.session.password = req.body.password
      req.session.user = result.rows[0]
      req.session.expired = new Date().getTime() + config.loginMaxAge
      next()
    }
  } catch (err) {
    console.log(err)
    logger.error(err.message)
    redirectError(res, req.query, loginError.AUTH_FAIL)
  }
})

const logout = function(req, res) {
  req.session = null
  res.redirect(config.serviceUrl)
}

module.exports = {
  verifyUserLogin,
  redirectError,
  redirectSuccess,
  verifySession,
  logout
}
