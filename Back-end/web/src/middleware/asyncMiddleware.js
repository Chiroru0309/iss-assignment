module.exports = fn => (req, res, next) => {
  Promise.resolve(fn(req, res, next)).catch(err => {
    console.log(err)
    res.set('Content-Type', 'application/json')
    res.status(500).send({ message: 'Internal server error.' })
  })
}
