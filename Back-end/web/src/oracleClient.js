const oracledb = require('oracledb')
const moment = require('moment')

oracledb.outFormat = oracledb.OUT_FORMAT_OBJECT
const time_format = 'MMMM Do YYYY'

module.exports = function(opts) {
  return {
    get_connection() {
      return oracledb.getConnection(opts)
    },
    getListEmployee() {
      const query = `SELECT
      emp.id,
      emp.username,
      emp.name,
      emp.birth,
      emp.email,
      emp.department_id,
      dept.name AS department,
      emp.position,
      emp.is_manager,
      emp.taxcode,
      emp.salary
      FROM hr.employee emp JOIN hr.department dept ON emp.department_id = dept.id`
      return oracledb.getConnection(opts).then(connection =>
        connection
          .execute(query)
          .then(result => {
            if (result.rows) {
              for (const item of result.rows) {
                item.BIRTH = moment(item.BIRTH).format(time_format)
                item.IS_MANAGER = item.IS_MANAGER == 1 ? 'YES' : 'NO'
              }
            }
            return result
          })
          .finally(() => {
            connection.close()
          })
      )
    },
    getEmployee(username) {
      const query = `SELECT
      emp.id,
      emp.username,
      emp.name,
      emp.birth,
      emp.email,
      emp.department_id,
      dept.name AS department,
      emp.position,
      emp.is_manager,
      emp.taxcode,
      emp.salary
      FROM hr.employee emp JOIN hr.department dept ON emp.department_id = dept.id WHERE emp.username='${username}'`
      return oracledb.getConnection(opts).then(connection =>
        connection
          .execute(query)
          .then(result => {
            console.log(result)
            if (result.rows) {
              for (const item of result.rows) {
                item.BIRTH = moment(item.BIRTH).format(time_format)
                item.IS_MANAGER = item.IS_MANAGER == 1 ? 'YES' : 'NO'
              }
            }
            return result
          })
          .finally(() => {
            connection.close()
          })
      )
    },
    addEmployee(employee) {
      const query = `INSERT INTO hr.employee(
        username
        ,name
        ,birth
        ,email
        ,department_id
        ,position
        ,is_manager
        ,taxcode
        ,salary
      ) VALUES (
        '${employee.username}'
        ,'${employee.name}'
        ,DATE '${moment(parseInt(employee.birth)).format('YYYY-MM-DD')}'
        ,'${employee.email}'
        ,${employee.department_id}
        ,'${employee.position}'
        ,${employee.is_manager ? 1 : 0}
        ,'${employee.taxcode}'
        ,${employee.salary}
      )`
      return oracledb.getConnection(opts).then(connection =>
        connection
          .execute(query)
          .then(result => connection.execute('COMMIT').then(ret => result))
          .finally(() => {
            connection.close()
          })
      )
    },
    removeEmployee(username) {
      const query = `DELETE FROM hr.employee WHERE username='${username}'`
      return oracledb.getConnection(opts).then(connection =>
        connection
          .execute(query)
          .then(result => connection.execute('COMMIT').then(ret => result))
          .finally(() => {
            connection.close()
          })
      )
    },
    updateEmployee(username, data) {
      const query = `UPDATE hr.employee SET
        username='${data.username}'
        ,name='${data.name}'
        ,birth=DATE '${moment(parseInt(data.birth)).format('YYYY-MM-DD')}'
        ,email='${data.email}'
        ,department_id=${data.department_id}
        ,position='${data.position}'
        ,is_manager=${data.is_manager ? 1 : 0}
        ,taxcode='${data.taxcode}'
        ,salary=${data.salary}
        WHERE username='${username}'
        `
      return oracledb.getConnection(opts).then(connection =>
        connection
          .execute(query)
          .then(result => connection.execute('COMMIT').then(ret => result))
          .finally(() => {
            connection.close()
          })
      )
    },
    removeUser(username) {
      const query = `
      BEGIN
        hr_sec.remove_employee('${username}');
      END;`
      return oracledb.getConnection(opts).then(connection =>
        connection
          .execute(query)
          .then(result => result)
          .finally(() => {
            connection.close()
          })
      )
    },
    addUser(username, password, dept_id, is_manager) {
      const query = `BEGIN
        hr_sec.add_user('${username}','${password}',${dept_id},${is_manager});
      END;`
      return oracledb.getConnection(opts).then(connection =>
        connection
          .execute(query)
          .then(result => result)
          .finally(() => {
            connection.close()
          })
      )
    },
    grantRole(username, dept_id, is_manager, is_revoke) {
      const query = `
      BEGIN
        hr_sec.grant_role('${username}',${dept_id},${is_manager},${is_revoke ? 1 : 0});
      END;`
      return oracledb.getConnection(opts).then(connection =>
        connection
          .execute(query)
          .then(result => result)
          .finally(() => {
            connection.close()
          })
      )
    }
  }
}
