/**
 * Router serving api for the front-end
 * @module router/api_router
 */
const express = require('express')
const config = require('../config')
const router = express.Router()
const asyncMiddleware = require('../middleware/asyncMiddleware')
const oracleClient = require('../oracleClient')
const jwt = require('jsonwebtoken')

const VerifyJWTToken = function(req, res, next) {
  const authorization = req.headers.authorization
  if (authorization) {
    const token = authorization.split(' ')[1]
    jwt.verify(token, config.jwtSecret, (err, decoded) => {
      if (!err) {
        req.username = decoded.user
        req.password = decoded.password
        req.dept_id = decoded.dept_id
        req.is_manager = decoded.is_manager
      } else {
        console.log(err)
      }
    })
  }
  next()
}

router.post(
  '/login',
  asyncMiddleware(async (req, res) => {
    const user = req.body.username
    const password = req.body.password
    const opts = {
      user,
      password,
      connectString: config.connectString
    }
    try {
      const result = await oracleClient(opts).getEmployee(user)
      if (result.rows.length > 0) {
        const data = result.rows[0]
        const token = jwt.sign(
          { user, password, dept_id: data.DEPARTMENT_ID, is_manager: data.IS_MANAGER },
          config.jwtSecret,
          { expiresIn: '1d' }
        )
        return res.status(200).send({ message: 'ok', token, data: result.rows })
      }
    } catch (err) {
      console.log(err)
    }
    return res.status(403).send({ message: 'Wrong username or password' })
  })
)

router.post(
  '/add_employee',
  VerifyJWTToken,
  asyncMiddleware(async (req, res) => {
    res.setHeader('Content-Type', 'application/json')
    let opts = {}
    if (req.username && req.password) {
      opts = {
        user: req.username,
        password: req.password,
        connectString: config.connectString
      }
    } else {
      opts = {
        user: req.session.username,
        password: req.session.password,
        connectString: config.connectString
      }
    }
    const employee = {
      username: req.body.username,
      password: req.body.password,
      name: req.body.name,
      birth: req.body.birth,
      email: req.body.email,
      department_id: req.body.department_id,
      position: req.body.position,
      is_manager: req.body.is_manager ? 1 : 0,
      taxcode: req.body.taxcode,
      salary: req.body.salary
    }
    try {
      await oracleClient(opts).addEmployee(employee)
      await oracleClient(config.hr_opts).addUser(
        employee.username,
        employee.password,
        employee.department_id,
        employee.is_manager
      )
      res.status(200).send({ message: 'ok' })
    } catch (err) {
      res.status(400).send({ message: err.message })
    }
  })
)

router.post(
  '/update_employee',
  VerifyJWTToken,
  asyncMiddleware(async (req, res) => {
    res.setHeader('Content-Type', 'application/json')
    let opts = {}
    if (req.username && req.password) {
      opts = {
        user: req.username,
        password: req.password,
        connectString: config.connectString
      }
    } else {
      opts = {
        user: req.session.username,
        password: req.session.password,
        connectString: config.connectString
      }
    }
    const employee = {
      username: req.body.username,
      name: req.body.name,
      birth: req.body.birth,
      email: req.body.email,
      department_id: req.body.department_id,
      position: req.body.position,
      is_manager: req.body.is_manager ? 1 : 0,
      taxcode: req.body.taxcode,
      salary: req.body.salary
    }
    try {
      await oracleClient(opts).updateEmployee(employee.username, employee)
      await oracleClient(config.hr_opts).grantRole(employee.username, employee.department_id, employee.is_manager, true)
      res.status(200).send({ message: 'ok' })
    } catch (err) {
      res.status(400).send({ message: err.message })
    }
  })
)

router.get(
  '/employee_info',
  VerifyJWTToken,
  asyncMiddleware(async (req, res) => {
    res.setHeader('Content-Type', 'application/json')
    let opts = {}
    let can_edit = false
    if (req.username && req.password) {
      opts = {
        user: req.username,
        password: req.password,
        connectString: config.connectString
      }
      can_edit = req.dept_id == 2
    } else {
      opts = {
        user: req.session.username,
        password: req.session.password,
        connectString: config.connectString
      }
      can_edit = req.session.user.DEPARTMENT_ID == 2
    }
    const username = req.query.username
    try {
      const result = await oracleClient(opts).getEmployee(username)
      res.status(200).send({ message: 'ok', data: result.rows, can_edit })
    } catch (err) {
      res.status(400).send({ message: err.message })
    }
  })
)

router.get(
  '/list_employees',
  VerifyJWTToken,
  asyncMiddleware(async (req, res) => {
    res.setHeader('Content-Type', 'application/json')
    let opts = {}
    let can_edit = false
    if (req.username && req.password) {
      opts = {
        user: req.username,
        password: req.password,
        connectString: config.connectString
      }
      can_edit = req.dept_id == 2
    } else {
      opts = {
        user: req.session.username,
        password: req.session.password,
        connectString: config.connectString
      }
      can_edit = req.session.user.DEPARTMENT_ID == 2
    }
    try {
      const result = await oracleClient(opts).getListEmployee()
      res.status(200).send({ message: 'ok', data: result.rows, can_edit })
    } catch (err) {
      res.status(400).send({ message: err.message })
    }
  })
)

router.post(
  '/remove_employee',
  VerifyJWTToken,
  asyncMiddleware(async (req, res) => {
    res.setHeader('Content-Type', 'application/json')
    let opts = {}
    if (req.username && req.password) {
      opts = {
        user: req.username,
        password: req.password,
        connectString: config.connectString
      }
    } else {
      opts = {
        user: req.session.username,
        password: req.session.password,
        connectString: config.connectString
      }
    }
    const username = req.body.username
    try {
      await oracleClient(opts).removeEmployee(username)
      await oracleClient(config.hr_opts).removeUser(username)
      res.status(200).send({ message: 'ok' })
    } catch (err) {
      res.status(400).send({ message: err.message })
    }
  })
)

module.exports = router
