const express = require('express')
const config = require('../config')
const router = express.Router()
const asyncMiddleware = require('../middleware/asyncMiddleware')
const oracleClient = require('../oracleClient')
const moment = require('moment')
const time_format = 'MMMM Do YYYY'

const admin_menu = {
  me: {
    name: 'My information',
    desc: '',
    icon: 'fa-user'
  },
  employees: {
    name: 'Employees',
    desc: 'Manage your employees',
    icon: 'fa-users'
  }
}

router.get('/', (req, res) => {
  res.redirect(req.originalUrl + '/me')
})

router.get(
  '/me',
  asyncMiddleware(async (req, res) => {
    const username = req.query.username
    let renderOpts = { user: req.session.user, info: req.session.user, menu: admin_menu, page_id: 'me' }
    if (username) {
      const opts = {
        user: req.session.username,
        password: req.session.password,
        connectString: config.connectString
      }
      const result = await oracleClient(opts).getEmployee(username)
      if (result.rows.length > 0) {
        renderOpts = { user: req.session.user, info: result.rows[0], menu: admin_menu, page_id: 'me' }
        return res.render('dashboard', renderOpts)
      }
      return res.render('403', renderOpts)
    }
    res.render('dashboard', renderOpts)
  })
)

router.get('/employees', (req, res) => {
  const page_id = 'employees'
  const renderOpts = { user: req.session.user, menu: admin_menu, page_id }
  res.render('dashboard', renderOpts)
})

module.exports = router
