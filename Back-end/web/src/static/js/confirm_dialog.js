function confirmDialog(opts, callback) {
  $(`<div id="confirmDialog" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content rounded shadow">
      <div class="modal-header" style="background: #3c8dbc">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3 id="confirmDialogTitle" class="modal-title" style="color: #fff">Title</h3>
      </div>
      <div class="modal-body">
        <h4 id="confirmDialogText">Are you sure ?</h4>
      </div>
      <div class="modal-footer">
        <button type="button" id="btnConfirmYes" class="btn btn-primary">Yes</button>
        <button type="button" id="btnConfirmNo" class="btn btn-default" data-dismiss="modal">No</button>
      </div>
      <div class="loading-overlay">
        <i class="fa fa-spinner fa-pulse fa-fw" id="loading-icon"></i>
      </div>
    </div>
  </div>
</div>`).appendTo('body')
  $('#confirmDialogTitle').text(opts.title)
  $('#confirmDialogText').text(opts.text)
  $('#btnConfirmYes').click(() => {
    callback(true)
    // $('#confirmDialog').modal('hide')
  })
  $('#confirmDialog').on('hidden.bs.modal', () => {
    $('#confirmDialog').remove()
  })
  $('#confirmDialog').modal('show')
}
