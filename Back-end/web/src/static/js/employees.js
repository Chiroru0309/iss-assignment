const time_format = 'MMMM Do YYYY, h:mm:ss A'

function fetchTableEmployees() {
  $.ajax({
    type: 'GET',
    url: '/api/list_employees',
    success(res, textStatus, jqXHR) {
      tableEmployees = $('#tableEmployees').DataTable()
      tableEmployees.clear().draw(false)
      let id = 0
      for (const item of res.data) {
        tableEmployees.row
          .add([
            item.ID,
            '<a href=/dashboard/me?username=' + item.USERNAME + '>' + item.USERNAME + '</a>',
            item.NAME,
            item.BIRTH,
            item.EMAIL,
            item.DEPARTMENT,
            item.POSITION,
            item.IS_MANAGER,
            item.TAXCODE,
            item.SALARY,
            res.can_edit
              ? '<button type="submit" class="btn btn-primary mr-2" onClick="editEmployee(\'' +
                id +
                '\')"><i class="fa fa-pencil"></i></button>' +
                '<button type="submit" class="btn btn-danger mr-2" onClick="removeEmployees(\'' +
                item.USERNAME +
                '\')"><i class="fa fa-trash"></i></button>'
              : ''
          ])
          .draw(false)
        id++
      }
    }
  })
}

function editEmployee(id) {
  const table = $('#tableEmployees').DataTable()
  const data = table.data()[id]
  $('#editEmployeeModal').modal('show')
  $('#inputNewUsername').val($(data[1]).text())
  $('#inputNewFullname').val(data[2])
  $('#inputNewBirthday').val(moment(data[3], 'MMMM Do YYYY').format('YYYY/MM/DD'))
  $('#inputNewEmail').val(data[4])
  $('#inputNewDeparment option')
    .filter(function() {
      console.log(data[5])
      return this.text === data[5]
    })
    .attr('selected', true)
  $('#inputNewIsmanager').prop('checked', data[7] === 'YES')
  $('#inputNewJobTitle').val(data[6])
  $('#inputNewTaxcode').val(data[8])
  $('#inputNewSalary').val(data[9])
}

function removeEmployees(username) {
  const dialogOpts = {
    title: 'Confirm to remove user',
    text: 'Are you sure to remove ' + username + ' ?'
  }

  confirmDialog(dialogOpts, ans => {
    if (ans) {
      $.ajax({
        type: 'POST',
        url: '/api/remove_employee',
        data: { username },
        success() {
          fetchTableEmployees()
          $('#confirmDialog').modal('hide')
        },
        error(jqXHR, textStatus, errorThrown) {
          confirmDialog({ title: 'Error', text: jqXHR.responseJSON.error }, ans => {})
          $('#confirmDialog').addClass('modal-danger')
        }
      })
    }
  })
}

$(() => {
  $('.input-date')
    .datepicker({
      autoclose: true,
      todayHighlight: true
    })
    .datepicker('update', new Date())
  const table = $('#tableEmployees').DataTable({
    paging: true,
    lengthChange: true,
    searching: false,
    ordering: true,
    info: true,
    autoWidth: true,
    scrollX: true
  })

  fetchTableEmployees()

  $('#addEmployeeModal').on('hidden.bs.modal', () => {
    $('#inputEmployeeForm').addClass('active')
    $('#resultAddEmployee').removeClass('active')
    $('#btnAddEmployee').show()
  })

  $('#editEmployeeModal').on('hidden.bs.modal', () => {
    $('#inputUpdateEmployeeForm').addClass('active')
    $('#resultUpdateEmployee').removeClass('active')
    $('#btnUpdateEmployee').show()
  })

  $('#btnAddEmployee').click(() => {
    $('.loading-overlay').addClass('active')
    const username = $('#inputUsername').val()
    const password = $('#inputPassword').val()
    const name = $('#inputFullname').val()
    const birth = moment($('#inputBirthday').val(), 'YYYY/MM/DD').valueOf()
    const email = $('#inputEmail').val()
    const position = $('#inputJobTitle').val()
    const department_id = $('#inputDeparment :selected').val()
    const department = $('#inputDeparment :selected').text()
    const is_manager = $('#inputIsmanager').val()
    const taxcode = $('#inputTaxcode').val()
    const salary = $('#inputSalary').val()
    $.ajax({
      type: 'POST',
      url: '/api/add_employee',
      data: { username, password, name, birth, email, department_id, position, is_manager, taxcode, salary },
      success(res, textStatus, jqXHR) {
        $('#inputEmployeeForm').removeClass('active')
        $('#resultAddEmployee').addClass('active')
        $('#btnAddEmployee').hide()
        $('#txtUsername').text(username)
        $('#txtFullname').text(name)
        $('#txtJobTitle').text(position)
        $('#txtDepartment').text(department)
        $('#txtPasswordText').text(res.data.password)
        fetchTableEmployees()
      },
      error(jqXHR, textStatus, errorThrown) {
        $('#inputEmployeeForm').prepend(
          '<div class="alert alert-danger alert-dismissible">' +
            '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' +
            '<h4><i class="icon fa fa-warning"></i>Error!</h4>' +
            jqXHR.responseJSON.message +
            '</div>'
        )
      },
      complete() {
        $('.loading-overlay').removeClass('active')
      }
    })
  })

  $('#btnUpdateEmployee').click(() => {
    $('.loading-overlay').addClass('active')
    const username = $('#inputNewUsername').val()
    const name = $('#inputNewFullname').val()
    const birth = moment($('#inputNewBirthday').val(), 'YYYY/MM/DD').valueOf()
    const email = $('#inputNewEmail').val()
    const position = $('#inputNewJobTitle').val()
    const department_id = $('#inputNewDeparment :selected').val()
    const department = $('#inputNewDeparment :selected').text()
    const is_manager = $('#inputNewIsmanager').val()
    const taxcode = $('#inputNewTaxcode').val()
    const salary = $('#inputNewSalary').val()
    $.ajax({
      type: 'POST',
      url: '/api/update_employee',
      data: { username, name, birth, email, department_id, position, is_manager, taxcode, salary },
      success(res, textStatus, jqXHR) {
        $('#inputUpdateEmployeeForm').removeClass('active')
        $('#resultUpdateEmployee').addClass('active')
        $('#btnUpdateEmployee').hide()
        $('#txtNewUsername').text(username)
        $('#txtNewFullname').text(name)
        $('#txtNewJobTitle').text(position)
        $('#txtNewDepartment').text(department)
        $('#txtNewPasswordText').text('Not update')
        fetchTableEmployees()
      },
      error(jqXHR, textStatus, errorThrown) {
        console.log('error')
        $('#inputUpdateEmployeeForm').prepend(
          '<div class="alert alert-danger alert-dismissible">' +
            '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' +
            '<h4><i class="icon fa fa-warning"></i>Error!</h4>' +
            jqXHR.responseJSON.message +
            '</div>'
        )
      },
      complete() {
        $('.loading-overlay').removeClass('active')
      }
    })
  })
})
