const time_format = 'MMMM Do YYYY, h:mm:ss A'

function fetchTableServiceAccounts() {
  $.ajax({
    type: "GET",
    url: '/api/all_service_accounts',
    success: function(res, textStatus, jqXHR) {
      tableServiceAccount = $('#tableServiceAccount').DataTable()
      tableServiceAccount.clear().draw(false)
      for (const item of res.data) {
        tableServiceAccount.row.add([
          item.account,
          item.service,
          moment(parseInt(item.last_update)).format(time_format),
          moment(parseInt(item.created_at)).format(time_format),
          item.account_enabled? 'Yes' : 'No',
          '<button type="submit" class="btn btn-success mr-2" onClick="resetServiceAccountPassword(\''+ item.account + '\',\'' + item.service + '\')"><i class="fa fa-refresh"></i></button>' +
          '<button type="submit" class="btn btn-danger mr-2" onClick="deleteServiceAccount(\''+ item.account + '\',\'' + item.service + '\')"><i class="fa fa-trash"></i></button>'
        ]).draw(false)
      }
    }
  })
}

function resetServiceAccountPassword(account, service) {

  $('#resetServiceAccountModal').modal('show')
  $('#btnResetSA').click(function() {
    $('#resetServiceAccountModal .loading-overlay').addClass('active')
    $.ajax({
      type: "POST",
      url: "/api/reset_service_account",
      data: { account, service },
      success: function(res, textStatus, jqXHR) {
        $('#resetServiceAccountModal .loading-overlay').removeClass('active')
        $('#confirmResetSA').removeClass('active')
        $('#resultResetSA').addClass('active')
        $('#btnResetSA').hide()
        $('#txtAccountResetSA').text(res.data.account)
        $('#txtServiceResetSA').text(res.data.service)
        $('#txtCreatedAtResetSA').text(moment(parseInt(res.data.last_update)).format(time_format))
        $('#txtPasswordTextResetSA').text(res.data.password)
        fetchTableServiceAccounts()
      },
      error: function() {

      }
    })
  })
}

function deleteServiceAccount(account, service) {

  const dialogOpts = {
    title: 'Confirm to delete',
    text: 'Are you sure to delete this service account?'
  }

  confirmDialog(dialogOpts, function(ans) {
    if (ans) {
      $.ajax({
        type: "POST",
        url: "/api/delete_service_account",
        data: { account, service },
        success: function() {
          fetchTableServiceAccounts()
          $('#confirmDialog').modal('hide')
        },
        error: function() {

        }
      })
    }
  })

}

$(function () {
  $('#tableServiceAccount').DataTable({
    'paging'      : true,
    'lengthChange': true,
    'searching'   : false,
    'ordering'    : true,
    'info'        : true,
    'autoWidth'   : false,
    'scrollX'     : true
  });
  fetchTableServiceAccounts()

  $('#createServiceAccountModal').on('hidden.bs.modal', function () {
    $('#inputServiceAccountForm').addClass('active')
    $('#resultServiceAccount').removeClass('active')
    $('#btnGenPwd').show()
  })

  $('#resetServiceAccountModal').on('hidden.bs.modal', function () {
    $('#confirmResetSA').addClass('active')
    $('#resultResetSA').removeClass('active')
    $('#btnResetSA').show()
  })

  $('#btnGenPwd').click(function() {
    $('.loading-overlay').addClass('active');
    const account = $('#inputAccount').val()
    const service = $('#inputService').val()
    $.ajax({
      type: "POST",
      url: "/api/create_service_account",
      data: { account, service },
      success: function(res, textStatus, jqXHR) {
        $('#inputServiceAccountForm').removeClass('active')
        $('#resultServiceAccount').addClass('active')
        $('#btnGenPwd').hide()
        $('#txtAccount').text(res.data.account)
        $('#txtService').text(res.data.service)
        $('#txtCreatedAt').text(moment(parseInt(res.data.created_at)).format(time_format))
        $('#txtPasswordText').text(res.data.password)
        fetchTableServiceAccounts()
      },
      error: function(jqXHR, textStatus, errorThrown) {
        $('#inputServiceAccountForm').prepend(
          '<div class="alert alert-danger alert-dismissible">' +
            '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' +
            '<h4><i class="icon fa fa-warning"></i>Error!</h4>' +
            jqXHR.responseJSON.message +
          '</div>')
      },
      complete: function() {
        $('.loading-overlay').removeClass('active');
      }
    })
  })

})
