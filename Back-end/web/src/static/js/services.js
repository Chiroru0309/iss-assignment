
$(function () {
  const tableAppPassword = $('#tableServiceAccount').DataTable({
    'paging'      : true,
    'lengthChange': true,
    'searching'   : false,
    'ordering'    : true,
    'info'        : true,
    'autoWidth'   : false
  });
})
