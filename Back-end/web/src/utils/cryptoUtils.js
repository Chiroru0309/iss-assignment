const crypto = require('crypto')

const encrypt = function(plaintext, key) {
  const iv = Buffer.alloc(16)
  const cipher = crypto.createCipheriv('aes-256-cbc', Buffer.from(key, 'hex'), iv)
  const encrypted = Buffer.concat([cipher.update(plaintext), cipher.final()])
  return encrypted.toString('hex')
}

const decrypt = function(ciphertext, key) {
  const iv = Buffer.alloc(16)
  const encryptedText = Buffer.from(ciphertext, 'hex')
  const decipher = crypto.createDecipheriv('aes-256-cbc', Buffer.from(key, 'hex'), iv)
  const decrypted = Buffer.concat([decipher.update(encryptedText), decipher.final()])
  return decrypted.toString()
}

const createHMAC = function(text, key) {
  return crypto
    .createHmac('sha1', Buffer.from(key, 'hex'))
    .update(text)
    .digest('hex')
}

const randomBytes = function(len) {
  return crypto.randomBytes(len).toString('hex')
}

module.exports = {
  encrypt,
  decrypt,
  createHMAC,
  randomBytes
}
