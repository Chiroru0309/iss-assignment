export const navItems = [
    {
      name: 'Trang chủ',
      url: '/trangchu',
      icon: 'icon-speedometer',
      badge: {
        variant: 'warning',
        text: 'NEW'
      }
    },
    {
      name: 'Quản lý phòng ban',
      url: '/quanlyphongban',
      icon: 'pi pi-download'
    }
    ,
    {
      name: 'Quản lý nhân viên',
      url: '/quanlynhanvien',
      icon: 'fa fa-user',
      badge: {
        variant: 'primary',
        text: 'Admin'
      }
    },
    {
      name: 'Về chúng tôi',
      url: '/gioithieu',
      icon: 'fa fa-info-circle'
    },
    {
      name: 'Liên hệ',
      url: '/lienhe',
      icon: 'pi pi-comments'
    },
    {
      name: 'FAQ',
      url: '/faq',
      icon: 'fa fa-question-circle'
    }
    // },
    // {
    //   name: 'Quản lý sản phẩm',
    //   url: '/sanphamAdmin',
    //   icon: 'fa fa-download',
    //   badge: {
    //     variant: 'primary',
    //     text: 'Admin'
    //   }
    // }
  ];