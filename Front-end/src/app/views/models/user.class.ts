export class User
{
    id: number;
    username: String;
    name: String;
    birth: Date;
    email: String;
    department_id: String;
    position:String;
    salary:number;
}