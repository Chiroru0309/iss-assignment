import { Injectable } from '@angular/core';
import {Http, Response,  Headers, RequestOptions, RequestMethod} from '@angular/http';
import {sanpham} from '../models/sanpham.class';
import { Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {HttpHeaders} from '@angular/common/http';
import { Options } from 'selenium-webdriver/firefox';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};



@Injectable({
  providedIn: 'root'
})
export class SanPhamAdminService {

  API_URL: string = 'http://localhost/api/product/read.php';
  constructor( public http: Http) { }
  getsanphamsSmall() {
    return this.http.get(this.API_URL);
  }

  putsanphams(sp: sanpham): Observable<sanpham>{
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
   return this.http.put('http://localhost/api/product/update.php', sp,options).pipe(map((res: Response) => res.json()));
  }

  postsanphams(sp: sanpham): Observable<sanpham>{
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.post('http://localhost/api/product/create.php',sp,options)
    .pipe(map(
      (
        res:Response)=> res.json()
      ));
  }

  deletesanphams(sp:sanpham):Observable<sanpham>{
    let headers = new Headers({'Content-Type':'application/json'});
    let options = new RequestOptions({headers: headers});
    return this.http.post('http://localhost/api/product/delete.php',sp,options).pipe(map((res:Response) => res.json()));
  }
}
